package pt.msoftware.spocktests.greetings;

/**
 * Created by usermpss on 16/05/2015.
 */
public class Greeting {
    private String name;
    protected String greeting = "Hello";

    public Greeting() {
    }

    public Greeting(String name) {
        this.name = name;
    }

    public Greeting(String greeting, String name) {
        this.greeting = greeting;
        this.name = name;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String sayHello() {
        return greeting + " " + name + "!";
    }
}
