package pt.msoftware.spocktests.greetings;

/**
 * Created by usermpss on 16/05/2015.
 */
public class FormalGreeting extends Greeting {
    public FormalGreeting(String name) {
        super(name);
        super.greeting = "Hi";
    }
}
