package pt.msoftware.spocktests.math;

/**
 * Created by usermpss on 16/05/2015.
 */
public class Math {
    public static int add(int num1, int num2) {
        return num1 + num2;
    }

    public static int subtract(int num1, int num2) {
        return num1 - num2;
    }

    public static int multiply(int num1, int num2) {
        return num1 * num2;
    }

    public static double divide(int num1, int num2) {
        return num1 / num2;
    }


}
