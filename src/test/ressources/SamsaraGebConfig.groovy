/**
 * Created by Marco Santos on 20.09.2019
 */
environments {
    local {
        urlWithoutForwardSlash = "http://localhost:4200/samsara"
        urlWithoutForwardSlash = urlWithoutForwardSlash.toLowerCase()
        urlWithForwardSlash = urlWithoutForwardSlash + "/"
    }

    test {
        urlWithoutForwardSlash = "http://sims3dev.robeco.nl/samsara"
        urlWithoutForwardSlash = urlWithoutForwardSlash.toLowerCase()
        urlWithForwardSlash = urlWithoutForwardSlash + "/"
    }

    /*
    test {
        urlWithoutForwardSlash = "https://twitter.com"
        urlWithoutForwardSlash = urlWithoutForwardSlash.toLowerCase()
        urlWithForwardSlash = urlWithoutForwardSlash + "/"
    }

    prod {
        urlWithoutForwardSlash = "https://twitter.com"
        urlWithoutForwardSlash = urlWithoutForwardSlash.toLowerCase()
        urlWithForwardSlash = urlWithoutForwardSlash + "/"
    }
    */
}
