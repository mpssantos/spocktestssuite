package pt.msoftware.spocktests.payments

/**
 * Created by Marco Santos on 20.09.2019
 */
interface PaymentGateway {
    boolean makePayment(BigDecimal amount)
}
