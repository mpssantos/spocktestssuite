package pt.msoftware.spocktests.payments

import spock.lang.Specification

/**
 * Created by Marco Santos on 20.09.2019
 */
class PaymentGatewaySpec extends Specification {
    def "Should return default value for mock"() {
        given:
            def paymentGateway = Mock(PaymentGateway)

        when:
            def result = paymentGateway.makePayment(12.99)

        then:
            !result
    }

    def "Should return true value for mock"() {
        given:
            def paymentGateway = Mock(PaymentGateway)
            paymentGateway.makePayment(20.3) >> true

        when:
            def result = paymentGateway.makePayment(20.3)

        then:
            result
    }
}
