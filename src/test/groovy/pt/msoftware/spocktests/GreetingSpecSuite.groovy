package pt.msoftware.spocktests

import org.junit.runner.RunWith
import org.junit.runners.Suite
import pt.msoftware.spocktests.greetings.FormalGreetingSpec
import pt.msoftware.spocktests.greetings.GreetingSpec
import spock.lang.Specification

/**
 * Created by usermpss on 16/05/2015.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses([GreetingSpec.class, FormalGreetingSpec.class])
class GreetingSpecSuite extends Specification {

}
