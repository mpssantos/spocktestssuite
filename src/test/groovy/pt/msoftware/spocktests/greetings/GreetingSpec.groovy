package pt.msoftware.spocktests.greetings;

import spock.lang.Specification

/**
 * Created by usermpss on 16/05/2015.
 */
class GreetingSpec extends Specification {
    def "Greeting Hello person"() {
        given:
            Greeting greeting = new Greeting()
        when:
            greeting.setName(name)
        then:
        expectedResult == greeting.sayHello()

        where:
        name        | expectedResult
        "Marco"     | "Hello Marco!"
        "Eunice"    | "Hello Eunice!"
    }

    def "Greeting defined person"() {
        given:
        Greeting greeting = new Greeting()

        when:
        greeting.setGreeting(greetingStr)
        greeting.setName(name)

        then:
        expectedResult.equals(greeting.sayHello());

        where:
        name        | greetingStr   | expectedResult
        "Marco"     | "Hi"          | "Hi Marco!"
        "Eunice"    | "Hola"        | "Hola Eunice!"
        "Pedro"     | "Bom dia"     | "Bom dia Pedro!"
    }
}
