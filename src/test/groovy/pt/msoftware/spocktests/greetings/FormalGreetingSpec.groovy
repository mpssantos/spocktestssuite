package pt.msoftware.spocktests.greetings
import spock.lang.Specification

/**
 * Created by usermpss on 16/05/2015.
 */
class FormalGreetingSpec extends Specification {
    def "Greeting Hi Marco"() {
        given:
            Greeting greeting = new FormalGreeting("Marco")
            def message
        when:
            message = greeting.sayHello()
        then:
            message == "Hi Marco!"
    }
}
