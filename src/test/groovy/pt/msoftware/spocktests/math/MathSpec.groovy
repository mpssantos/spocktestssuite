package pt.msoftware.spocktests.math

import java.lang.Math
import spock.lang.Specification

/**
 * Created by usermpss on 17/05/2015.
 */
class MathSpec extends Specification {
    def "one plus one should be two"() {
        expect:
            1+1 == 2
    }

    def "addition test"() {
        given:
            def num1 = 2
            def num2 = 2

        when:
            def add = num1 + num2
        then:
            add == 4
    }

    /*Data Driven Tests: Parameterized tests*/

    def "numbers to the power of two"(int a, int b, int c) {
        expect:
            Math.pow(a, b) == c

        where:
            a | b | c
            1 | 2 | 1
            2 | 2 | 4
            3 | 2 | 9
            4 | 2 | 16
    }
}
