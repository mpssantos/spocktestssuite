package pt.msoftware.spocktests.gebtests.samsara

import geb.spock.GebReportingSpec
import pt.msoftware.spocktests.gebtests.samsara.pages.LoginPage
import spock.lang.Stepwise

/**
 * Created by Marco Santos on 20.09.2019
 */

@Stepwise
class LoginLogoutSpec extends GebReportingSpec {
    def setupSpec() {
        cfg = ConfigReader.getConfiguration()
        LoginPage.config = cfg

//        TwitterPage.config = cfg
//        username = cfg.loginFlowConfig.username
//        password = cfg.loginFlowConfig.password
    }

    def cleanupSpec() {
    }

    def setup() {
        baseUrl = cfg.urlWithForwardSlash
    }

    def "Login with valid username and password"() {
        given: "You are on Login page"
            to LoginPage

        LoginPage loginPage = at LoginPage
    }
}
