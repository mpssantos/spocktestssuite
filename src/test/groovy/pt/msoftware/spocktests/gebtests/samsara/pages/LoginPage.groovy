package pt.msoftware.spocktests.gebtests.samsara.pages
import geb.Page

/**
 * Created by Marco Santos on 20.09.2019
 */
class LoginPage extends Page {

    static ConfigObject config

    static url = "login"

    private static final FORM_TITLE = "Login"

    static content = {
//        loginFormContainer() {$('.signin-wrapper')}
//        loginFormTitle() {loginFormContainer.find('h1').first()}
//        loginForm()  { loginFormContainer.find('form.js-signin') }
//        usernameField()  { loginForm.find('.js-username-field') }
//        passwordField()  { loginForm.find('.js-password-field') }
//        loginButton() {loginForm.find('.submit')}
    }

    static at = {
        //loginFormTitle.text().toUpperCase().contains(FORM_TITLE)
        browser.getCurrentUrl().toLowerCase().startsWith(config.urlWithForwardSlash + url)
        //loginButton.isDisplayed()
    }

    def enterLoginPassword(login, password){
        usernameField.value(login)
        passwordField.value(password)
    }

    def clickOnLoginButton(){
        //loginButton.click()
    }
}
