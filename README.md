# **Spock Tests Suite Project** #
===============================

The purpose of this project is to help you get started with Spock Tests Suite. At this stage this project have 3 different test groovy classes and one suite that includes only two of them.

The project include build script for Manven that will automatically download all required dependencies, compile the project, and finally run the example specs. 

**### Prerequisites ###**

- JDK 5 or higher
- Maven 2.x (for Maven build)

**### How do I get set up? ###**

To run the tests use one of the following profiles

- spock - This profile will run all the spock tests
- suite - This profile will run the suite tests defined in the class GreetingSpecSuite.groovy

command examples:

mvn clean install - This command will run the default profile that will execute the Junit tests, that in this project are non existent

mvn clean install -Pspock - This command will run all the spock tests

mvn clean install -Psuite - This command will run all the spock tests defined in the suite GreetingSpecSuite

**### Who do I talk to? ###**

* Repo owner or admin